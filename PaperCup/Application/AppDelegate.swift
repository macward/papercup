//
//  AppDelegate.swift
//  PaperCup
//
//  Created by Mac Ward on 6/20/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Firebase
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //Fabric.with([Crashlytics.self])
        FIRApp.configure()
        
        
        if FIRAuth.auth()?.currentUser != nil {
            
            if UserDefaults.standard.value(forKey: "access_token") != nil {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyBoard.instantiateViewController(withIdentifier: "initialViewController")
                self.window?.rootViewController = initialViewController
            }
            
        } else {
            
            FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
                if user!.isAnonymous == true {
                    if UserDefaults.standard.value(forKey: "access_token") != nil {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let initialViewController = storyBoard.instantiateViewController(withIdentifier: "initialViewController")
                        self.window?.rootViewController = initialViewController
                    }
                }
            })
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: - TODO: refactor slack redirection
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if url.scheme == "ppcup" {
            let current = self.window?.rootViewController
            current?.dismiss(animated: true, completion: nil)
            
            let slackClient = SlackClient(clientID: "69196332950.69673318578", clientSecret: "046a5c6e96fec91dc6c7b411e06f4590")
            
            let code = slackClient.getUserCode(url.absoluteString)
            
            slackClient.getAccessToken(code, requestSuccess: { (response) in
                
                UserDefaults.standard.setValuesForKeys(response)
                
                
                if UserDefaults.standard.value(forKey: "access_token") != nil {
                    // MARK: CONTROL AUTH
                    FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
                        if user!.isAnonymous == true {
                            // MARK: TODO  Animate this transition
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let initialViewController = storyBoard.instantiateViewController(withIdentifier: "initialViewController")
                            self.window?.rootViewController = initialViewController
                        }
                    })
                
                }
            }) { (error) in
                print(error)
            }
        }
        
        return false
    }
}

