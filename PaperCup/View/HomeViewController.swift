//
//  HomeViewController.swift
//  PaperCup
//
//  Created by Mac Ward on 6/22/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit
import SWRevealViewController
import AVFoundation

class HomeViewController: UIViewController {
    
    
    @IBOutlet weak var recordButton: RecordButton!
    var deltadBi: Float = 0.0
    var recordViewModel:RecorderViewModel!
    
    let lblChatName = UILabel()
    let menuBtn = UIButton()
    
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.recordViewModel = RecorderViewModel(teamID: UserDefaults.standard.value(forKey: "team_id") as! String)
        
        self.recordViewModel.recording.delegate = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.actOnSpecialNotification),
                                               name: NSNotification.Name(rawValue: "Notification"),
                                               object: nil)
        
        if self.revealViewController() != nil {
            self.revealViewController().rearViewRevealWidth = self.view.frame.width - 45
            self.revealViewController().rearViewRevealOverdraw = 0.0
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        self.menuBtn.frame = CGRect(x: 10, y: 0, width: 38, height: 38)
        self.menuBtn.makeRounded = true
        self.menuBtn.backgroundColor = paintWith(color: .wineColor)
        
        self.menuBtn.addTarget(self, action: #selector(self.openSidemenu), for: UIControlEvents.touchUpInside)
        
        self.navigationController?.navigationBar.addSubview(self.menuBtn)
        
        self.lblChatName.frame = CGRect(x: 58, y: 8, width: 200, height: 20)
        self.lblChatName.text = ApplicationManager.shared.currentChat?.name
        self.navigationController?.navigationBar.addSubview(lblChatName)
        
        recordButton.delegate = self
    }
    
    func updateView() {
        self.lblChatName.text = ApplicationManager.shared.currentChat?.name
    }
    
    func openSidemenu(_ sender: Any) {
        self.revealViewController().revealToggle(animated: true)
    }
    
    func actOnSpecialNotification(notification: NSNotification) {
        self.revealViewController().revealToggle(animated: true)
        self.updateView()
    }
    
    @IBAction func playAudio(_ sender: Any) {
        self.recordViewModel.playAudio()
    }
    
    @IBAction func uploadFile(_ sender: Any) {
        //self.recordViewModel.copyAndStore()
        guard let destID = ApplicationManager.shared.currentChat?.ID else {return}
        guard let meID = UserDefaults.standard.value(forKey: "user_id") else {return}
        self.recordViewModel.saveMessage(meID: meID as! String, destID: destID)
    }
    
}

extension HomeViewController: RecordingControlDelegate {
    
    func didStartRecording() {
        if self.recordViewModel.isRecording == false {
            self.recordViewModel.startRecording()
        }
    }
    
    func didFinishRecording() {
        self.recordViewModel.stopRecording()
    }
}

extension HomeViewController: RecorderDelegate {
    
    func audioMeterDidUpdate(_ dB: Float) {
        if counter == 0 {
            counter += 1
            if dB != self.deltadBi {
                var delta = dB - self.deltadBi
                self.deltadBi = dB
                if delta < 0 {
                    delta = delta * -1
                }
                self.recordButton.updateHUD(variant: CGFloat(delta))
            }
        } else {
            if counter == 6 {
                counter = 0
            } else {
                counter += 1
            }
        }
    }
}
