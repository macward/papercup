//
//  NavViewController.swift
//  PaperCup
//
//  Created by Mac Ward on 6/22/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit

enum ConversationType {
    case MEMBER, CHANNEL
}

class NavViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SidebarDelegate{

    @IBOutlet weak var tableView: UITableView!
    
    let navSectionTitles = ["CHANNELS", "DIRECT MESSAGES"]
    
    var navElements = [String:[AnyObject]]()

    lazy var members = [Member]()
    lazy var channels = [Channel]()
    
    lazy var auxMembers = [Member]()
    
    let viewModel = NavViewModel()
    
    @IBOutlet weak var tfSearch: SearchTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "ChannelsTableViewCell", bundle: nil), forCellReuseIdentifier: "channelCell")
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = paintWith(color: .wineColor)
        
        self.tfSearch.delegate = self
        
        self.viewModel.delegate = self
        
        self.viewModel.getMembers()
        self.viewModel.getChannels()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return navSectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return channels.count
        } else {
            return members.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ChannelsTableViewCell!
        
        if indexPath.section == 0 {
        
            cell = self.tableView.dequeueReusableCell(withIdentifier: "channelCell", for: indexPath) as! ChannelsTableViewCell
            cell.channelName = channels[indexPath.row].name
            cell.cellType = CellType.CHANNEL
        
        } else if indexPath.section == 1 {
            
            cell = self.tableView.dequeueReusableCell(withIdentifier: "channelCell", for: indexPath) as! ChannelsTableViewCell
            cell.channelName = members[indexPath.row].realName
            cell.cellType = CellType.MEMBER
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        if cell != nil {
            cell?.isSelected = true
        }
        
        var dataTransfer: [AnyHashable: Any]!
        
        if indexPath.section == 0 {
            dataTransfer = ["object":channels[indexPath.row], "type":ConversationType.CHANNEL]
        } else {
            dataTransfer = ["object":members[indexPath.row],"type":ConversationType.MEMBER]
        }
        
        self.viewModel.serCurrentChat(index: indexPath)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Notification"), object: nil, userInfo: dataTransfer)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if cell != nil {
            cell?.isSelected = false
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return navSectionTitles[section]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func membersListSuccessfullyUpdated(memberList: [Member]) {
        self.members = memberList
        DispatchQueue.main.async { [unowned self] in
            self.tableView.reloadData()
        }
    }
    
    func channelListSuccessfullyUpdated(channelList: [Channel]) {
        self.channels = channelList
        DispatchQueue.main.async { [unowned self] in
            self.tableView.reloadData()
        }
        
    }
}

extension NavViewController: UITextFieldDelegate {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.tfSearch.resignFirstResponder()
    }
    
    // MARK: - Text field delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.auxMembers = self.members
        self.tableView.reloadData()
        print("start editing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.tfSearch.resignFirstResponder()
        self.members = self.auxMembers
        self.tableView.reloadData()
        print("end editing")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let result = self.auxMembers.filter { (item) -> Bool in
            print(textField.text)
            return item.name.contains(textField.text!)
        }
        
        print(result)
        
        self.members = result
        self.tableView.reloadData()
        return true
    }
}
