//
//  ViewController.swift
//  NextJob
//
//  Created by Mac Ward on 7/14/16.
//  Copyright © 2016 Mac Ward. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
    }
    
    @IBAction func showSlackAuth(_ sender: AnyObject) {
        let slackAuthModal = SlackOauthUIViewController()
        self.present(slackAuthModal, animated: true, completion: nil)
    }

}

