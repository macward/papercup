//
//  ChannelsTableViewCell.swift
//  PaperCup
//
//  Created by Mac Ward on 6/22/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit

enum CellType {
    case MEMBER, CHANNEL
}

class ChannelsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelChannelName: UILabel!
    @IBOutlet weak var selectedView: UIView!
    
    var badgeCountValue: Int = 0 {
        didSet {
            if badgeCountValue > 0 {
                self.badgeValue.text = String(badgeCountValue)
                self.badgeView.isHidden = false
            }
        }
    }
    
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeValue: UILabel!
    @IBOutlet weak var statusBadge: UIView!
    @IBOutlet weak var groupSharp: UILabel!
    
    var isGroup = true
    
    var cellType: CellType! = nil {
        didSet {
            if cellType == .MEMBER {
                self.statusBadge.isHidden = false
                self.groupSharp.isHidden = true
            } else if cellType == .CHANNEL {
                self.groupSharp.isHidden = false
                self.statusBadge.isHidden = true
            }
        }
    }
    
    override var isSelected: Bool {
        didSet{
            if (isSelected) {
                self.selectedView.backgroundColor = paintWith(color: .wineLite).withAlphaComponent(1)
            } else {
                self.selectedView.backgroundColor = paintWith(color: .wineLite).withAlphaComponent(0)
            }
        }
    }
    
    var channelName: String = "" {
        didSet {
            self.labelChannelName.text = channelName
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.selectedView.layer.cornerRadius = 6
        self.selectedView.backgroundColor = paintWith(color: .wineLite).withAlphaComponent(1)
        
        statusBadge.isHidden = true
        groupSharp.isHidden = true
        badgeView.isHidden = true
    }
    
    

    override func layoutIfNeeded() {
        drawLine()
    }
    
    
    func drawLine () {
        //design the path
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: self.frame.height))
        path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        
        //design path in layer
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = paintWith(color: .wineColor).cgColor
        shapeLayer.lineWidth = 1.0
        self.layer.addSublayer(shapeLayer)
    }
    
    
}
