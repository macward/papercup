//
//  Message.swift
//  PaperCup
//
//  Created by Mac Ward on 6/27/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation

struct Message {
    
    var teamID      : String = ""
    var sender      : String = ""
    var duration    : Float  = 0.0
    var date        : Date   = Date()
    var audioData   : Any
}
