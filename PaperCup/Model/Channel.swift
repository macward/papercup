//
//  Channel.swift
//  PaperCup
//
//  Created by Mac Ward on 6/23/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation

struct Channel {
    
    var teamID  : String    = ""
    var ID      : String    = ""
    var name    : String    = ""
    var members : [String]  = [String]()
    
    init(ID: String, name: String, members: [String]) {
        self.ID = ID
        self.name = name
        self.members = members
    }
    
}
