//
//  Chat.swift
//  PaperCup
//
//  Created by Mac Ward on 6/27/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation
import UIKit

struct ChatObject {
    
    var teamID      : String    = ""
    var ID          : String    = ""
    var name        : String    = ""
    var type        : Int       = 0
    var date        : Date?    = nil
    var imageURL    : String    = ""
    var image       : UIImage?  = UIImage()    
}
