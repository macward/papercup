//
//  Member.swift
//  PaperCup
//
//  Created by Mac Ward on 6/23/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation
import UIKit

import Alamofire
import AlamofireImage


struct Member {
    
    var teamID      : String  = ""
    var ID          : String  = ""
    var name        : String  = ""
    var realName    : String  = ""
    var imageUrl    : String  = ""
    var image       : UIImage = UIImage()
    
    init (ID: String, teamID: String, name: String, realName: String, image: UIImage?) {
        
        self.ID = ID
        self.teamID = teamID
        self.name = name
        self.realName = realName
        if let userImage = image {
            self.image = userImage
        }
        
    }

}
