//
//  ApiManager.swift
//  Papercup
//
//  Created by Mac Ward on 8/21/16.
//  Copyright © 2016 Mac Ward. All rights reserved.
//

import Foundation

import SwiftyJSON
import Alamofire


class ApiManager: ApiClient {
    
    static let SharedInstance = ApiManager()
    

    func userList(_ requestSuccess: @escaping (JSON) -> Void, requestFailure: @escaping (NSError) -> Void) -> Void {
        
        let url : String = ""
        
        self.get(url, success: { (response) in
            requestSuccess(response)
        }) { (error) in
            requestFailure(error)
        }
        
    }
    
    
    
    
}
