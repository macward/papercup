//
//  ApiClient.swift
//  Custom Libs
//
//  Created by Mac Ward on 7/27/16.
//  Copyright © 2016 Mac Ward. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

/**
 
 Crea un string con la url valida de la request
 
 - parameter urlPath: path del metodo a llamar
 
 - returns: url valida. String
 */

public func ApiUrl(_ urlPath : String) -> String {
    
    let apiUrl : String = "http://tapp.starredsolutions.com.ar/"
    
    return apiUrl + urlPath;
    
}

public struct ApiResponseStatus {
    static let Success = "success"
    static let Failure = "fail"
}

class ApiClient: NSObject {
    
    public var authHeaderValue : [String : String]?
    
    /**
     Create a GET Request: this is a wrapper for ApiRequest method
     
     - parameter url: the url request.
     - parameter succes: This is called when the request finished and has a successfully response.
     - parameter failure: This is called when the occur an error with the request.
     
     - returns : Void
     */
    
    func get(_ url: String, success: @escaping (JSON) -> Void, failure : @escaping (NSError) -> Void) -> Void {
        
        self.apiRequest(HTTPMethod.get, endpoint: url, params: nil, success: { (response) in
            success(response)
        }) { (error) in
            
            failure(error)
            
        }
    }
    
    /**
     Create a GET Request: this is a wrapper for ApiRequest method.
     
     - parameter url: the url request.
     - parameter params: This is a dictionary [String : Any] with the data to be send.
     - parameter succes: This is called when the request finished and has a successfully response.
     - parameter failure: This is called when the occur an error with the request.
     
     
     - returns : Void
     */
    
    func get(_ url: String, params : [String : AnyObject]?, success: @escaping (JSON) -> Void, failure : @escaping (NSError) -> Void) -> Void {
        
        self.apiRequest(HTTPMethod.get, endpoint: url, params: params, success: { (response) in
            
            success(response)
            
        }) { (error) in
            
            failure(error)
            
        }
    }
    
    /**
     Create a POST Request: this is a wrapper for ApiRequest method.
     
     - parameter url: the url request.
     - parameter params: This is a dictionary [String : Any] with the data to be send.
     - parameter succes: This is called when the request finished and has a successfully response.
     - parameter failure: This is called when the occur an error with the request.
     
     - returns : Void
     */
    
    func post(_ url: String, params : [String : AnyObject]?, success: @escaping (JSON) -> Void, failure: @escaping (NSError) -> Void) -> Void {
        
        self.apiRequest(HTTPMethod.post, endpoint: url, params: params, success: { (response) in
            
            success(response)
            
        }) { (error) in
            
            failure(error)
            
        }
        
    }
    
    /**
     Create a PUT Request: this is a wrapper for ApiRequest method.
     
     - parameter url: the url request. (String)
     - parameter params: This is a dictionary [String : Any] with the data to be send.
     - parameter succes: This is called when the request finished and has a successfully response.
     - parameter failure: This is called when the occur an error with the request.
     
     - returns : Void
     */
    
    func get (_ url: String, params : [String : AnyObject], success: @escaping (JSON) -> Void, failure: @escaping (NSError) -> Void) -> Void {
        self.apiRequest(HTTPMethod.put, endpoint: url, params: params, success: { (response) in
            
            success(response)
            
        }) { (error) in
            
            failure(error)
            
        }
    }
    
    /**
     Create a DELETE Request: this is a wrapper for ApiRequest method.
     
     - parameter url: the url request. (String)
     - parameter params: This is a dictionary [String : Any] with the data to be send.
     - parameter succes: This is called when the request finished and has a successfully response.
     - parameter failure: This is called when the occur an error with the request.
     
     - returns : Void
     */
    
    func delete (_ url: String, success: @escaping (JSON) -> Void, failure: @escaping (NSError) -> Void) -> Void {
        self.apiRequest(HTTPMethod.delete, endpoint: url, params: nil, success: { (response) in
            
            success(response)
            
        }) { (error) in
            
            failure(error)
            
        }
    }
    
    /**
     Create a request instance
     
     - parameter method: El metodo de la request. HTTPMethod
     - parameter url: the url request. (String)
     - parameter params: This is a dictionary [String : Any] with the data to be send.
     - parameter succes: This is called when the request finished and has a successfully response.
     - parameter failure: This is called when the occur an error with the request.
     
     - returns : Void
     */
    
    func apiRequest (_ method: HTTPMethod,  endpoint: String, params: [String : AnyObject]!, success: @escaping (JSON) -> Void, failure: @escaping (NSError) -> Void) -> Void {
        
        Alamofire.request(endpoint, method: method, parameters: params, encoding: JSONEncoding.default, headers:  self.authHeader()).responseJSON { (responseObject) in
            
            if responseObject.result.isSuccess {
                let responseJson = JSON(responseObject.result.value!)
                success(responseJson)
            }
            
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error! as NSError
                failure(error);
            }
        }
    }
    
    /**
     Verfifica que exista un access token y le da un formato
     
     - returns : [String : String] Es un diccionario.
     */
    
    func authHeader() -> [String : String]? {
        //return ["Authorization" : "Basic ZmVjZjU0OWEtNDM5Zi00YjVkLWI0OGMtMTVhMTRmY2EzMzJl"];
        
        return authHeaderValue;
    }
    
}
