//
//  ConnectionHandler.swift
//  PaperCup
//
//  Created by Mac Ward on 7/25/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation
import Firebase

class ConnectionHandler {
    
    var db:FIRDatabaseReference! = nil
    var user: FIRUser! = nil
    
    init(withRef db: FIRDatabaseReference, user: FIRUser?) {
        self.db = db
        self.user = user
    }
    
    func getFavorites(completion: @escaping ([String:AnyObject]?)->Void) {
        let favRef = self.db.child("users").child(self.user.uid).child("favorites")
        favRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.value != nil {
                completion(snapshot.value as? [String:AnyObject])
            }
            completion(nil)
        })
    }
    
    func getAudios(completion: @escaping([String:AnyObject]?)->Void) {
        
    }
    
    func storeAudio(completion: @escaping([String:AnyObject]?)-> Void) {
        
    }
    
}
