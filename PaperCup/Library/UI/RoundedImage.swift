//
//  RoundedImage.swift
//  PaperCup
//
//  Created by Mac Ward on 6/21/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit

class RoundedImage: UIImageView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.clipsToBounds = true
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = self.bounds.width / 2
    }
}
