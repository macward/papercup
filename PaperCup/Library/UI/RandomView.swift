//
//  RandomView.swift
//  PaperCup
//
//  Created by Mac Ward on 6/21/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit

class RandomView: UIView {
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.frame.size = self.randomSize()
        self.layer.cornerRadius = self.bounds.width / 2
        self.backgroundColor = randomStarColor()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func randomSize () -> CGSize {
        let ratio = (arc4random() % 8) + 2
        let size = CGSize(width: CGFloat(ratio), height: CGFloat(ratio))
        return size
    }
}
