//
//  RoundedButton.swift
//  PaperCup
//
//  Created by Mac Ward on 6/21/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    
}
