//
//  SearchTextField.swift
//  PaperCup
//
//  Created by Mac Ward on 6/22/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit

class SearchTextField: UITextField {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderWidth = 0.0
        
        let attrString = NSAttributedString(string: "Search", attributes: [NSForegroundColorAttributeName:UIColor.white])
        self.attributedPlaceholder = attrString
    }
    
    
    override func layoutSubviews() {
        self.roundCorners(radius: self.frame.height/2)
    }
    
    func roundCorners(radius:CGFloat) {
        
        let maskPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius)
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        
        self.layer.mask = maskLayer
        
        let frameLayer = CAShapeLayer()
        frameLayer.frame = bounds
        frameLayer.path = maskPath.cgPath
        frameLayer.strokeColor = UIColor.darkGray.cgColor
        
        frameLayer.fillColor = paintWith(color: .wineLite).cgColor
        
        self.layer.insertSublayer(frameLayer, at: 0)
    }
    
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
}
