//
//  PaperNavBar.swift
//  PaperCup
//
//  Created by Mac Ward on 6/23/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit
import SWRevealViewController

class PaperNavBar: UINavigationBar {

    let statusBadge = UIView()
    let menuButton = UIButton()
    let lblTeamName = UILabel()
    
    var teamName: String = "" {
        didSet {
            self.lblTeamName.text = teamName
        }
    }
    
    let viewHeigth: CGFloat = 65
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: viewHeigth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
       
    }

    
    
}
