//
//  SlackClient.swift
//  Papercup
//
//  Created by Mac Ward on 8/16/16.
//  Copyright © 2016 Mac Ward. All rights reserved.
//

import UIKit
import SwiftyJSON

class SlackClient: ApiClient {
    
    var oAuthAccessUrl = "https://slack.com/api/oauth.access?"
    
    var clientID = ""
    var clientSecret = ""
    
    init (clientID: String, clientSecret: String) {
        self.clientID = clientID
        self.clientSecret = clientSecret
    }
    
    func oAuthRequest (_ scope : [String], redirectUrl : String = "") -> URLRequest {
        let urlStr = "https://slack.com/oauth/authorize?client_id=\(clientID)&scope=" + scope.joined(separator: "+")
        let request = URLRequest(url: URL(string: urlStr)!)
        return request;
    }
    
    func getUserCode(_ responseUrl : String) -> String {
        
        if responseUrl.range(of: "code=") != nil {
            let segments = responseUrl._split(separator: "=")
            let tokenSegment = String(segments[1])?.components(separatedBy: "&")
            
            return tokenSegment![0]
            
        }
        return ""
    }
    
    func getAccessToken(_ code : String, requestSuccess: @escaping ([String: AnyObject]) -> Void, requestFailure: @escaping (NSError) -> Void) -> Void {
        oAuthAccessUrl += "client_id=\(clientID)&client_secret=\(clientSecret)&code=\(code)"
        
        self.get(oAuthAccessUrl, success: { (response) in
            var dict = [String: AnyObject]()
            for (key, value) : (String, JSON) in response {
                dict[key] = value.object as AnyObject?
            }
            
            requestSuccess(dict)
            
            /*
             {
             "team_name" : "PaperCup",
             "ok" : true,
             "user_id" : "U215SUU80",
             "scope" : "identify,im:history,channels:read,files:read,im:read,users:read,users:read.email,users.profile:read,chat:write:user,files:write:user,im:write,users:write",
             "team_id" : "T215S9STY",
             "access_token" : "xoxp-69196332950-69196980272-70069725792-230a7f81b6"
             }
             */
            
        }) { (error) in
            requestFailure(error)
        }
    }

}
