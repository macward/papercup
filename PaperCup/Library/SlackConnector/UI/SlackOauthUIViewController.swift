//
//  SlackOauthUIViewController.swift
//  PaperCup
//
//  Created by Mac Ward on 6/21/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit

class SlackOauthUIViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var slackWebView: UIWebView!
    let overlay = UIView()
    var overlaySize : CGRect! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scope : [String] = ["users:read",
                                "users:write",
                                "chat:write:user"]
        
        self.overlay.backgroundColor = paintWith(color: .wineColor)
        
        self.view.addSubview(overlay)
        
        
        let slackClient = SlackClient(clientID: "69196332950.69673318578", clientSecret: "046a5c6e96fec91dc6c7b411e06f4590")
        let request = slackClient.oAuthRequest(scope, redirectUrl: "")
        
        slackWebView.delegate = self
        slackWebView.loadRequest(request)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        let ratio = self.view.frame.height + 140
        self.overlaySize = CGRect(x: (self.view.frame.width / 2) - ratio / 2, y: (self.view.frame.height / 2) - ratio / 2, width: ratio, height: ratio)
        self.overlay.frame = overlaySize
        self.overlay.layer.masksToBounds = true
        self.overlay.layer.cornerRadius = ratio / 2
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.overlay.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }) { (completed) in
            self.overlay.removeFromSuperview()
        }
        
    }
}
