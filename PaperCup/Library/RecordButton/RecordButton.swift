//
//  RecordButton.swift
//  RecordButton
//
//  Created by Mac Ward on 7/25/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit

@objc protocol RecordingControlDelegate {
    func didStartRecording()
    func didFinishRecording()
    
    @objc optional func didCollideWithObject(collision: Bool)
}


public enum RecordButtonState : Int {
    case recording, iddle
}

class RecordButton: UIButton {
    
    var delegate: RecordingControlDelegate? = nil
    var currentState: RecordButtonState = RecordButtonState.iddle

    fileprivate var collider: UIView?
    
    fileprivate var saveAudio = true
    
    fileprivate var ring1 = CALayer()
    fileprivate var ring2 = CALayer()
    fileprivate var ring3 = CALayer()
    
    let ringSettings:[[String:CGFloat]] = [["ratio":2.2, "alpha":0.3],
                                           ["ratio":1.85, "alpha":0.2],
                                           ["ratio":1.44, "alpha":0.1]]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addTarget(self, action: #selector(didTouchUp), for: UIControlEvents.touchUpOutside)
        addTarget(self, action: #selector(didTouchUp), for: UIControlEvents.touchUpInside)
        addTarget(self, action: #selector(didTouchDown), for: UIControlEvents.touchDown)
        
        self.configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addTarget(self, action: #selector(didTouchUp), for: UIControlEvents.touchUpOutside)
        addTarget(self, action: #selector(didTouchUp), for: UIControlEvents.touchUpInside)
        addTarget(self, action: #selector(didTouchDown), for: UIControlEvents.touchDown)
        
        self.configure()
    }
    
    // MARK: - Configure Button
    func configure() {
        self.layer.cornerRadius = self.frame.height / 2
        
        self.ring1 = self.configureRing(withAlpha: ringSettings[0]["alpha"]!)
        self.ring2 = self.configureRing(withAlpha: ringSettings[1]["alpha"]!)
        self.ring3 = self.configureRing(withAlpha: ringSettings[2]["alpha"]!)
    
        self.layer.insertSublayer(self.ring1, at: 0)
        self.layer.insertSublayer(self.ring2, at: 1)
        self.layer.insertSublayer(self.ring3, at: 2)
        
    }
    
    func configureRing(withAlpha alpha: CGFloat)->CALayer {
        let layer = CALayer()
        layer.frame = self.bounds
        layer.bounds.size.width = self.bounds.width
        layer.bounds.size.height = self.bounds.height
        layer.backgroundColor = UIColor.blue.withAlphaComponent(alpha).cgColor
        layer.cornerRadius = layer.bounds.width / 2

        return layer
    }
    
    // MARK: - Gestures and Events
    @objc func didTouchDown() {
        self.currentState = .recording
        self.delegate?.didStartRecording()
        self.alpha = 0.8
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.2)
        self.ring1.transform = CATransform3DScale(CATransform3DIdentity, ringSettings[0]["ratio"]!, ringSettings[0]["ratio"]!, 1)
        self.ring2.transform = CATransform3DScale(CATransform3DIdentity, ringSettings[1]["ratio"]!, ringSettings[1]["ratio"]!, 1)
        self.ring3.transform = CATransform3DScale(CATransform3DIdentity, ringSettings[2]["ratio"]!, ringSettings[2]["ratio"]!, 1)
        CATransaction.commit()
        
        // print button status
        print(self.currentState)
    }
    
    @objc func didTouchUp() {
        self.currentState = .iddle
        self.delegate?.didFinishRecording()
        self.alpha = 1
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.2)
        self.ring1.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        self.ring2.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        self.ring3.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        CATransaction.commit()
        
        if self.saveAudio == true {
            print("record audio")
        } else {
            print("delete audio")
        }
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: [], animations: {
            //self.center = self.superview!.center
        }, completion: nil)
        
        // print button status
        print(self.currentState)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        return
        if touches.first != nil {
            let touch = touches.first!
            self.center = touch.location(in: self.superview)
            
            guard let rectCollider = self.collider else {return}
            
            if self.frame.intersects(rectCollider.frame) == true {
                self.saveAudio = false
                self.delegate?.didCollideWithObject!(collision: true)
            } else {
                self.saveAudio = true
                self.delegate?.didCollideWithObject!(collision: false)
            }
        }
    }
    
    // MARK: - Collision detection
    func setCollider (collider: UIView) {
        self.collider = collider
    }
    
    func updateHUD ( variant: CGFloat ){
        var offset = CGFloat(variant) * 0.1
        if offset > 3 {
            offset = 0.3
        }
        let scale1 = self.ringSettings[0]["ratio"]! + offset
        let scale2 = self.ringSettings[1]["ratio"]! + offset
        let scale3 = self.ringSettings[2]["ratio"]! + offset
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.3)
        self.ring1.transform = CATransform3DScale(CATransform3DIdentity, scale1, scale1, 1)
        self.ring2.transform = CATransform3DScale(CATransform3DIdentity, scale2, scale2, 1)
        self.ring3.transform = CATransform3DScale(CATransform3DIdentity, scale3, scale3, 1)
        CATransaction.commit()
    }
}

