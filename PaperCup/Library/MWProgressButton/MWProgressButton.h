//
//  MWProgressButton.h
//  runnerapp
//
//  Created by mac ward on 25/5/16.
//  Copyright © 2016 Mac Ward. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface MWProgressButton : UIControl
@property (nonatomic, strong) IBInspectable UIColor *foregroundColor;
@property (nonatomic, strong) IBInspectable UIColor *highlightedColor;
@end
