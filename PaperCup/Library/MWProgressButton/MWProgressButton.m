//
//  MWProgressButton.m
//  runnerapp
//
//  Created by mac ward on 25/5/16.
//  Copyright © 2016 Mac Ward. All rights reserved.
//

#import "MWProgressButton.h"

@interface MWProgressButton ()
@property (strong, nonatomic) CALayer * backingLayer;
@property (strong, nonatomic) CAShapeLayer * loadingLine;
@property (strong, nonatomic) CAGradientLayer *gradientLayer;
@property (strong, nonatomic) UIImageView * iconImage;
@property (strong, nonatomic) CALayer * iconLayer;

@end

@implementation MWProgressButton

#pragma mark - init methods

-(instancetype) init
{
    self = [super init];
    if (self){
        [self basicSetup];
    }
    return self;
}

-(instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self basicSetup];
    }
    return self;
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self basicSetup];
        
    }
    return self;
}

#pragma mark - custom init methods

-(void) basicSetup
{
    self.backgroundColor = [UIColor clearColor];
    self.backingLayer = [[CALayer alloc] init];
    [self.layer addSublayer:self.backingLayer];
    
    self.loadingLine = [CAShapeLayer layer];
    [self.layer addSublayer:self.loadingLine];
    
    
}

#pragma mark - Draw methods
-(void)drawRect:(CGRect)rect
{
    self.backingLayer.backgroundColor = self.foregroundColor.CGColor;
    self.backingLayer.cornerRadius = CGRectGetHeight(self.bounds) / 2;
    
    // gradient mask
    self.gradientLayer = [CAGradientLayer layer];
    self.gradientLayer.startPoint = CGPointMake(0.7,0.0);
    self.gradientLayer.endPoint = CGPointMake(0.7,1.0);
    self.gradientLayer.frame = self.bounds;
    NSMutableArray *colors = [NSMutableArray array];
    
    [colors addObject:(id)[UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0].CGColor];
    [colors addObject:(id)[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0].CGColor];
    self.gradientLayer.colors = colors;
    [self.gradientLayer setMask:self.backingLayer];
    [self.layer addSublayer:self.gradientLayer];
    // end gradient mask
    
    [self drawLoaderLine];
    
    self.iconImage = [[UIImageView alloc] initWithFrame:CGRectInset(self.bounds, 30, 30)];
    self.iconImage.image = [UIImage imageNamed:@"man_running_icon"];
    self.iconImage.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.iconImage];
    
}

#pragma mark - Layout methods

-(void) layoutSubviews{
    [super layoutSubviews];
    self.backingLayer.frame = self.bounds;
    
    self.loadingLine.frame = self.bounds;
}

#pragma mark - user interactions
- (void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    if (!highlighted){
        self.backingLayer.backgroundColor = self.foregroundColor.CGColor;
        self.loadingLine.opacity = 0;
        [[UIApplication sharedApplication]endIgnoringInteractionEvents];
        
        self.iconImage.alpha = 1;
        
    } else {
        [[UIApplication sharedApplication]beginIgnoringInteractionEvents];
        self.backingLayer.backgroundColor = self.highlightedColor.CGColor;
        
        self.iconImage.alpha = 0.6;
        
        self.loadingLine.opacity = 1;
        CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        drawAnimation.duration            = 1.5;
        drawAnimation.repeatCount         = 1.0;
        drawAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
        drawAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
        drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        [self.loadingLine addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
    }
}

# pragma mark - color setup
- (UIColor *)foregroundColor
{
    return _foregroundColor != nil ? _foregroundColor : [UIColor colorWithRed:43/255.0 green:189/255.0 blue:224/255.0 alpha:1.0];
}

- (UIColor *)highlightedColor
{
    return _highlightedColor != nil ? _highlightedColor : [UIColor colorWithRed:108/255.0 green:164/255.0 blue:176/255.0 alpha:1.0];
}

# pragma mark - custom drawing
-(void) drawLoaderLine
{
    CGRect offsetBounds = CGRectInset(self.loadingLine.bounds, -20, -20);
    CGPoint center = CGPointMake(CGRectGetWidth(self.bounds) / 2, CGRectGetHeight(self.bounds) / 2);
    UIBezierPath * path = [[UIBezierPath alloc] init];
    [path addArcWithCenter:center
                    radius:CGRectGetWidth(offsetBounds) / 2
                startAngle:1.5 * M_PI
                  endAngle:3.5 * M_PI
                 clockwise:YES];
    
    self.loadingLine.path = path.CGPath;
    self.loadingLine.fillColor = [UIColor clearColor].CGColor;
    self.loadingLine.strokeColor = [UIColor whiteColor].CGColor;
    self.loadingLine.lineWidth = 1.3;
    self.loadingLine.opacity = 0;
    [self.loadingLine setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:3],nil]];
}

@end
