//
//  RecorderViewModel.swift
//  PaperCup
//
//  Created by Mac Ward on 6/26/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation
import AVFoundation
import SwiftySound
import Firebase

class RecorderViewModel: NSObject {
    
    var recording: Recording!
    var isRecording  = false
    
    fileprivate var teamID: String = "" // este es el ID
    
    var directory: String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    init(teamID: String) {
        super.init()
        self.teamID = teamID
        createRecorder()
    }
    
    
    // este metodo crea una instancia del objeto Recording
    // y configura correctamente
    func createRecorder() {
        recording = Recording(to: "baserecord.m4a")
        DispatchQueue.global().async {
            do {
                try self.recording.prepare()
            } catch {
                print(error)
            }
        }
    }
    
    // este metodo comienza con una grabacion de audio
    func startRecording() {
        do {
            try recording.record()
            isRecording = true
        } catch {
            print(error)
        }
    }
    
    // este metodo detiene la grabacion de audio
    func stopRecording() {
        recording.stop()
        isRecording = false
    }
    
    // este metodo comenzara a reproducir
    // el audio grabado en el archivo "baserecord.m4a"
    func playAudio() {
        do {
            try recording.play()
        } catch {
            // handle error
            print(error)
        }
    }
    
    
    // guarda el archivo en firebase storage
    // y guarda los datos del mensaje en firebase database
    func saveMessage (meID:String, destID: String) {
        
        // esta es la url donde se encuentra el archivo base de audio
        // en este archivo se graba el audio y luego se crea una copia con un nombre diferente
        // y es subido a firebase
        let url = URL(fileURLWithPath: self.directory).appendingPathComponent("baserecord.m4a")
        
        // creo un nombre random para el archivo de audio
        let filename = "\(String.randomString(length: 10)).m4a"
        
        
        // la referencia al storage de firebase
        let storage = FIRStorage.storage().reference(forURL: "gs://papercup-e0981.appspot.com")
        let ref = storage.child("audios/\(filename)")
        
        // Intenta guardar el archivo en firebase storage
        ref.putFile(url, metadata: nil) { (storageData, error) in
            if error != nil {
                // handle error
            }
            
            
            // si no hay datos de respuesta, se corta la ejecucion.
            // MARK: TODO - hay que manejar los errores o las fallas de datos
            guard let fileData = storageData else {return}
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            
            // creo un diccionario con los datos a guardar
            let messageDict = ["teamID":self.teamID,
                               "sender": meID,
                               "duration":0.30,
                               "date": dateFormatter.string(from: Date()),
                               "fileUrl": fileData.downloadURL()!.absoluteString] as [String : Any]
            
            // -- ESTOS SON LOS MODELOS DONDE SE GUARDAN LOS MENSAJES EN FIREBASE
            // https://{firebase_url}/{TEAM_ID}/{DEST_ID}/{FROM_ID}/{ids}
            // https://{firebase_url}/{TEAM_ID}/{FROM_ID}/{DEST_ID}/{ids}
            // ------
            
            // creo una referencia a la base de datos de firebase
            let dbRef = FIRDatabase.database().reference()
            
            let destPath = dbRef.child(self.teamID).child(destID).child(meID)   // el path donde se guardara el mensaje en el destinatario
            let myPath = dbRef.child(self.teamID).child(meID).child(destID)     // este es el path donde se guardara el mensaje que envie
            
            destPath.childByAutoId().ref.setValue(messageDict) { (error, firRef) in
                print("data saved")
            }
            
            myPath.childByAutoId().ref.setValue(messageDict) { (error, firRef) in
                print("data saved")
            }
        }
    }
}
