//
//  NavViewModel.swift
//  PaperCup
//
//  Created by Mac Ward on 6/23/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit
import SlackKit


protocol SidebarDelegate {
    func membersListSuccessfullyUpdated(memberList: [Member])
    func channelListSuccessfullyUpdated(channelList: [Channel])
}


class NavViewModel: NSObject {
    
    fileprivate var channelList = [Channel]()
    fileprivate var membersList = [Member]()
    
    fileprivate var slackApi: WebAPI!
    
    
    var delegate: SidebarDelegate?
    
    
    override init() {
        super.init()
        self.slackApi = WebAPI(token: UserDefaults.standard.value(forKey: "access_token") as! String)
    }
    
    func getMembers() {
        slackApi.usersList(success: { (apiResponse) in
            if apiResponse != nil {
                for item in apiResponse! {
                    let member = Member(ID: item["id"] as! String,
                                        teamID: item["team_id"] as! String,
                                        name: item["name"] as! String,
                                        realName: item["real_name"] as! String,
                                        image: nil)
                    
                    self.membersList.append(member)
                }
                ApplicationManager.shared.members = self.membersList
                self.delegate?.membersListSuccessfullyUpdated(memberList: self.membersList)
            }
        }, failure: { (error) in
            print(error)
        })
    }
    
    func getChannels() {
        self.slackApi.channelsList(success: { (apiResponse) in
            if apiResponse != nil {
                for item in apiResponse! {
                    let channel = Channel(ID: item["id"] as! String, name: item["name"] as! String, members: item["members"] as! [String])
                    self.channelList.append(channel)
                }
                ApplicationManager.shared.channels = self.channelList
                self.delegate?.channelListSuccessfullyUpdated(channelList: self.channelList)
            }
        }) { (error) in
            print(error)
        }
    }
    
    
    func serCurrentChat (index: IndexPath) {
        if index.section == 0 {
            let item = channelList[index.row]
            
            var chat = ChatObject()
            chat.teamID = item.teamID
            chat.ID = item.ID
            chat.name = item.name
            chat.type = ChatType.CHANNEL.rawValue
            
            ApplicationManager.shared.currentChat = chat
        } else {
            let item = membersList[index.row]
            
            var chat = ChatObject()
            chat.teamID = item.teamID
            chat.ID = item.ID
            chat.name = item.name
            chat.type = ChatType.CHANNEL.rawValue
            
            ApplicationManager.shared.currentChat = chat
        }
    }
    
    
    
}
