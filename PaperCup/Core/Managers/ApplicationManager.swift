//
//  ApplicationManager.swift
//  PaperCup
//
//  Created by Mac Ward on 6/27/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import UIKit

class ApplicationManager: NSObject {
    
    static let shared = ApplicationManager()
    
    var members = [Member]()
    var channels = [Channel]()
    
    var currentChat: ChatObject? = nil
}
