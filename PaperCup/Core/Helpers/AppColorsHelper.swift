//
//  AppColorsHelper.swift
//  Tapp
//
//  Created by Mac Ward on 4/3/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation
import UIKit

enum AppColor {
    case purple, purpleDark, wineColor, wineLite, darkgray, lightgray, green, gray
}

func paintWith (color colorName: AppColor) -> UIColor {
    
    var colorValue : UIColor!
    
    switch colorName {
    case .purple:
        colorValue = UIColor(red:0.30, green:0.16, blue:0.52, alpha:1.0)
        break
    case .purpleDark:
        colorValue = UIColor(red:0.18, green:0.15, blue:0.21, alpha:1.0)
        break
    case .wineColor:
        colorValue = UIColor(red:0.18, green:0.15, blue:0.21, alpha:1.0)
        break
    case .darkgray:
        colorValue = UIColor(red:0.30, green:0.16, blue:0.52, alpha:1.0)
        break
    case .lightgray:
        colorValue = UIColor(red:0.20, green:0.27, blue:0.36, alpha:1.0)
        break
    case .green:
        colorValue = UIColor(red:0.31, green:0.82, blue:0.29, alpha:1.0)
        break
    case .gray:
        colorValue = UIColor(red:0.44, green:0.46, blue:0.50, alpha:1.0)
        break
    case .wineLite:
        colorValue = UIColor(red:0.19, green:0.17, blue:0.24, alpha:1.0)
        break
    }
    
    return colorValue
}

extension UIColor {
    
    class func randomColor() -> UIColor {
        
        let hue : CGFloat = CGFloat(arc4random() % 256) / 256 // use 256 to get full range from 0.0 to 1.0
        let saturation : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from white
        let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from black
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }
    
}

func randomStarColor() -> UIColor {
    let colors = [  UIColor(red:0.98, green:0.62, blue:0.62, alpha:1.0), // rosa
                    UIColor(red:0.73, green:0.73, blue:1.00, alpha:1.0), // celeste
                    UIColor(red:0.93, green:0.89, blue:0.89, alpha:1.0), // cream
                    UIColor(red:0.97, green:0.91, blue:0.11, alpha:1.0), // yellow
                    UIColor(red:0.64, green:0.94, blue:0.93, alpha:1.0), // verde agua
                    UIColor(red:0.45, green:0.93, blue:0.56, alpha:1.0), // verde
                    UIColor(red:0.91, green:0.47, blue:1.00, alpha:1.0) // pink
    ]
    
    let index = arc4random_uniform(UInt32(colors.count))
    return colors[Int(index)]
}

// UIColor(red:0.98, green:0.62, blue:0.62, alpha:1.0) // rosa
// UIColor(red:0.73, green:0.73, blue:1.00, alpha:1.0) // celeste
// UIColor(red:0.93, green:0.89, blue:0.89, alpha:1.0) // cream
// UIColor(red:0.97, green:0.91, blue:0.11, alpha:1.0) // yellow
// UIColor(red:0.64, green:0.94, blue:0.93, alpha:1.0) // verde agua
// UIColor(red:0.45, green:0.93, blue:0.56, alpha:1.0) // verde
// UIColor(red:0.91, green:0.47, blue:1.00, alpha:1.0) // pink
