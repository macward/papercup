//
//  StringExtensions.swift
//  PaperCup
//
//  Created by Mac Ward on 6/27/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation

extension String {
    static func randomString(length: Int) -> String {
        let charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var c = charSet.characters.map { String($0) }
        var s:String = ""
        for _ in (1...length) {
            s.append(c[Int(arc4random()) % c.count])
        }
        return s
    }
}
