//
//  UIViewExtension.swift
//  PaperCup
//
//  Created by Mac Ward on 6/23/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    @IBInspectable var makeRounded: Bool {
        get {
            return self.makeRounded
        }
        set {
            if newValue == true {
                self.layer.cornerRadius = self.frame.width / 2
            }
        }
    }
    
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
        }
    }
}

 
