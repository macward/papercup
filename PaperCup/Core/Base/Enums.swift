//
//  Enums.swift
//  PaperCup
//
//  Created by Mac Ward on 6/27/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import Foundation

enum ChatType: Int{
    case CHANNEL=1, MEMBER
}
