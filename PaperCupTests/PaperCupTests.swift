//
//  PaperCupTests.swift
//  PaperCupTests
//
//  Created by Mac Ward on 6/20/17.
//  Copyright © 2017 Mac Ward. All rights reserved.
//

import XCTest
@testable import PaperCup

class PaperCupTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    // Tests para RecorderViewModel
    
    // 1- Test record
    // 2- Test recorder flow
    //      - create record
    //      - start/stop recording
    //      - save file
    
    // 3- RecorderViewModel Initialization
    func test_RecorderInitSuccess() {}
    func test_RecorderInitFailure() {}
    
    // 4- Save file in firebase
    func test_SaveFileSuccessfully() {}
    func test_SaveFileFailure() {}
    func test_SaveEmptyFileError() {}
    func test_AudioFileDoNotExist() {}
}
